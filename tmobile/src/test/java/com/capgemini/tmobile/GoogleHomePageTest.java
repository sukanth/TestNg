package com.capgemini.tmobile;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.MalformedURLException;
import java.util.List;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.capgemini.beans.TestData;
import com.capgemini.utility.Driver;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

/**
 * 
 * @author sugunda
 *
 */
public class GoogleHomePageTest {

	private WebDriver driver;
	private final static Logger log = Logger.getLogger(GoogleHomePageTest.class);
	private GoogleHomePage googleHomePage;

	@AfterTest
	public void shutDownSelenium() {
		driver.quit();
	}

	@DataProvider(name = "dataForDrivers")
	public Object[][] getData() throws FileNotFoundException {
		JsonElement jsonData = new JsonParser().parse(new FileReader("src/test/resources/testData.json"));
		JsonElement dataSet = jsonData.getAsJsonObject().get("testData");
		List<TestData> testData = new Gson().fromJson(dataSet, new TypeToken<List<TestData>>() {
		}.getType());
		Object[][] returnValue = new Object[testData.size()][1];
		int index = 0;
		for (Object[] each : returnValue) {
			each[0] = testData.get(index++);
		}
		return returnValue;
	}

	@Test(dataProvider = "dataForDrivers")
	public void openGoogleHomePageWithDifferentBrowsersTest(TestData testdata) throws MalformedURLException {
		log.info("Entered Method: com.capgemini.tmobile.GoogleHomePageTest.openGoogleHomePageTest()");
		driver = Driver.getDriver(testdata.getBrowserName(), testdata.getOperatingSystem(), testdata.getVersion(),
				testdata.getJobName());
		googleHomePage = new GoogleHomePage(driver);
		googleHomePage.navigateTo("https://www.google.com/");
		googleHomePage.searchString("tmobile");
		googleHomePage.clickSearch();
		log.info("Exit com.capgemini.tmobile.GoogleHomePageTest.openGoogleHomePageTest()");
	}
}
