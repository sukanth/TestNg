package com.capgemini.beans;
/**
 * 
 * @author sugunda
 *
 */
public class TestData {
	private String browserName;
	private String operatingSystem;
	private String version;
	private String jobName;
	public String getBrowserName() {
		return browserName;
	}
	public void setBrowserName(String browserName) {
		this.browserName = browserName;
	}
	public String getOperatingSystem() {
		return operatingSystem;
	}
	public void setOperatingSystem(String operatingSystem) {
		this.operatingSystem = operatingSystem;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	@Override
	public String toString() {
		return "TestData [browserName=" + browserName + ", operatingSystem=" + operatingSystem + ", version=" + version
				+ ", jobName=" + jobName + "]";
	}
}
