package com.capgemini.utility;

import java.net.MalformedURLException;
import java.net.URL;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.capgemini.tmobile.GoogleHomePageTest;

/**
 * 
 * @author sugunda
 * @version 1.0
 *
 */
public class Driver {
	private final static Logger log = Logger.getLogger(GoogleHomePageTest.class);
	public static final String USERNAME = "sukanth";
	public static final String ACCESS_KEY = "3afb4755-e9a7-4310-bd50-a702795d333b";
	public static final String URL = "http://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.saucelabs.com:80/wd/hub";
	private static DesiredCapabilities capabilities;
	
	public  static WebDriver getDriver(String browserName,String platform,String version,String jobName) throws MalformedURLException {
		log.info("Entered getDriver() with parameters" +"Browser Name: "+ browserName +" platform: "+platform+" version: "+version);
		capabilities = new DesiredCapabilities();
		log.info("Setting Capabilities");
		capabilities.setCapability(CapabilityType.BROWSER_NAME, browserName);
		capabilities.setCapability(CapabilityType.PLATFORM, platform);
		capabilities.setCapability(CapabilityType.VERSION, version);
		capabilities.setCapability("name", jobName);
		log.info("Exit getDriver()");
		return new RemoteWebDriver(new URL(URL), capabilities);
	}

}
