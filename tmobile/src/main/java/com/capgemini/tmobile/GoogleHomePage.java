package com.capgemini.tmobile;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * 
 * @author sugunda
 *
 */
public class GoogleHomePage extends BasePage{
	protected WebDriver driver;

	public GoogleHomePage(WebDriver driver) {
		super(driver);
		this.driver = driver;
	}

	private final Logger log = Logger.getLogger(GoogleHomePage.class);

	@FindBy(id = "lst-ib")
	private WebElement searchBar;

	@FindBy(name = "btnK")
	private WebElement googleSearchButton;

	
	protected GoogleHomePage searchString(String searchString) {
		log.info("Search String is :" + searchString);
		searchBar.sendKeys(searchString);
		return this;
	}
	
	protected GoogleHomePage clickSearch() {
		jsClick(googleSearchButton);
		return this;
	}
}
