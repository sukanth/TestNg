package com.capgemini.tmobile;
/**
 * @author sugunda
 */
import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class BasePage {
	private final Logger log = Logger.getLogger(BasePage.class);

	protected WebDriver driver;

	public BasePage(WebDriver driver) {
		super();
		this.driver = driver;
		PageFactory.initElements(driver,this);
	}

	protected void navigateTo(String url) {
		driver.get(url);
		log.info("Navigated to: " + url);
	}

	protected void jsClick(WebElement ele) {
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele);
		log.info("Click performed on element" + ele);
	}
}
